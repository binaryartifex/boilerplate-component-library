import React from "react";
import { render } from "../../utils/test-utils";

import { Button } from "./button";

describe("Component", () => {
	it("renders without crashing", () => {
		const component = render(<Button label="Click Me" />);
		expect(component).toBeTruthy();
	});
});
