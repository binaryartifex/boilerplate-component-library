import React from "react";
import { ButtonProps } from "./button.types";

export const Button = (props: ButtonProps) => {
	return <button className="bg-neutral-500">{props.label}</button>;
};

export default Button;
