import React from "react";
import { Story, Meta } from "@storybook/react";
import { Button } from "./button";
import { ButtonProps } from "./button.types";

export default {
	title: "Design System/Form Fields/Button",
	component: Button,
} as Meta;

export const Primary: Story<ButtonProps> = (args) => <Button {...args} />;
Primary.args = {
	label: "Click Here",
};
