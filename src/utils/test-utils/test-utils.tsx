/* eslint-disable import/export */
import React, { ReactElement, ReactNode } from "react";
import {
	render,
	RenderOptions,
	queries,
	RenderResult,
} from "@testing-library/react";

function ComponentProviders({ children }: { children: ReactNode }) {
	return <div>{children}</div>;
}

const customRender = (
	ui: ReactElement,
	options?: Omit<RenderOptions, "wrapper">
): RenderResult<typeof queries, HTMLElement, HTMLElement> =>
	render(ui, { wrapper: ComponentProviders, ...options });

export * from "@testing-library/react";
export { customRender as render };
