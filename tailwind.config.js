const { join } = require("path");
const colors = require("tailwindcss/colors");

/** @type {import('tailwindcss').Config} */
module.exports = {
	content: [
		join(__dirname, "./src/**/*.{js,ts,jsx,tsx,html}"),
		join(__dirname, "./.storybook/preview.js"),
	],
	theme: {
		borderRadius: {
			none: 0,
			DEFAULT: "10px",
			full: "50%",
		},
		colors: {
			transparent: "transparent",
			current: "currentColor",
			white: "#FFFFFF",
			primary: colors.sky,
			error: colors.red,
			neutral: colors.slate,
			success: colors.emerald,
			warning: colors.amber,
		},
		dark: "class",
		fontFamily: {
			body: ["Nunito", "sans-serif"],
		},
		fontSize: {
			xxs: ["0.750rem", { lineHeight: "19px" }],
			xs: ["0.875rem", { lineHeight: "23px" }],
			sm: ["1.000rem", { lineHeight: "26px" }],
			md: ["1.125rem", { lineHeight: "29px" }],
			lg: ["1.500rem", { lineHeight: "32px" }],
			xl: ["1.875rem", { lineHeight: "41px" }],
			"2xl": ["2.250rem", { lineHeight: "49px" }],
			"3xl": ["3.000rem", { lineHeight: "66px" }],
			"4xl": ["3.750rem", { lineHeight: "82px" }],
		},
		screens: {
			sm: "640px",
			md: "768px",
			lg: "1024px",
			xl: "1280px",
		},
	},
	plugins: [require("@tailwindcss/forms")],
};
