/** @type {import('ts-jest/dist/types').InitialOptionsTsJest} */
module.exports = {
	clearMocks: true,
	moduleDirectories: ["node_modules", "src"],
	moduleFileExtensions: ["js", "ts", "tsx", "json", "node"],
	moduleNameMapper: {
		".+\\.(css|styl|less|sass|scss|png|jpg|svg|ttf|woff|woff2)$":
			"identity-obj-proxy",
	},
	passWithNoTests: true,
	preset: "ts-jest",
	roots: ["src"],
	testEnvironment: "jest-environment-jsdom",
	testMatch: ["**/?(*.)+(spec|test).[tj]s?(x)"],
	testPathIgnorePatterns: ["\\\\node_modules\\\\"],
};
