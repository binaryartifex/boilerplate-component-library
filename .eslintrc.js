module.exports = {
	env: {
		browser: true,
		es2021: true,
		jest: true,
	},
	extends: [
		"plugin:react/recommended",
		"standard",
		"plugin:tailwindcss/recommended",
		"plugin:storybook/recommended",
		"prettier",
	],
	parser: "@typescript-eslint/parser",
	parserOptions: {
		ecmaFeatures: {
			jsx: true,
		},
		ecmaVersion: "latest",
		sourceType: "module",
	},
	plugins: ["react", "@typescript-eslint", "tailwindcss"],
	root: true,
	rules: {},
	settings: {
		react: {
			version: "detect",
		},
	},
};
